/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Robert Beckett <bob.beckett@collabora.com>
 * Author: Corentin Noël <corentin.noel@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstdlib>

#include <pps/gpu/gpu_datasource.h>

int main(int argc, const char **argv)
{
    using namespace pps;

    // Connects to the system tracing service
    perfetto::TracingInitArgs args;
    args.backends = perfetto::kSystemBackend;
    perfetto::Tracing::Initialize(args);

    std::string driver_name =
        (argc > 1) ? gpu::Driver::find_driver_name(argv[1]) : gpu::Driver::default_driver_name();
    gpu::GpuDataSource::RegisterDataSource(driver_name);

    while (true) {
        gpu::GpuDataSource::Trace(gpu::GpuDataSource::trace_callback);
    }

    return EXIT_SUCCESS;
}
